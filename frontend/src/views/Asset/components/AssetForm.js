import React from 'react';

import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';

const AssetForm = props =>{
	return(
			<React.Fragment>
			<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
			>
				<ModalHeader
				toggle={props.handleShowForm}
				style={{"backgroundColor":"pink", "color":"white"}}
				>
				Add Asset
				</ModalHeader>
					<ModalBody>
						<FormInput
						label={"Asset Name"}
						type={"text"}
						name={"name"}
						placeholder={"Enter Name"}
						required={props.nameRequired}
						onChange={props.handleAssetNameChange}
						/>

						<FormInput
						label={"Asset Description"}
						type={"text"}
						name={"description"}
						placeholder={"Enter Description"}
						required={props.descriptionRequired}
						onChange={props.handleAssetDescriptionChange}
						/>

						<FormInput
						label={"Asset Category"}
						type={"text"}
						name={"category"}
						placeholder={"Enter Category"}
						required={props.categoryRequired}
						onChange={props.handleAssetCategoryChange}
						/>

						<FormInput
						label={"Asset Stock"}
						type={"number"}
						name={"stock"}
						placeholder={"Enter Stock Quantity"}
						required={props.stockRequired}
						onChange={props.handleAssetStockChange}
						/>

						<Button color="warning"
						onClick={props.handleSaveAsset}
						disabled={props.name!=="" 
						&& props.description !=="" 
						&& props.category !== "" 
						&& props.stock!==""
						&& props.stock >0 ? false : true}
						>Add Asset</Button>


					</ModalBody>	

			</Modal>
			</React.Fragment>


		)
}

export default AssetForm;