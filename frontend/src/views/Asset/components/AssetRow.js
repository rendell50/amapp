import React, {useState} from 'react';
import {
	Button,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem

} from 'reactstrap';

import {FormInput} from '../../../globalcomponents';


const AssetRow = props => {

	const [dropdownOpen, setDropdownOpen] = useState(false);

	const toggle = () => setDropdownOpen(!dropdownOpen);
	
	const asset = props.asset;

	return (
		<React.Fragment>
		
		<tr>
			<td>{asset.serialNumber}</td>
				<td
			onClick={()=>props.handleNameEditInput(asset._id)}
			>{props.showName && props.editId === asset._id
				? 
				<FormInput
					defaultValue={asset.name}
					type={"text"}
					onBlur={(e)=>props.handleEditName(e, asset._id)}
				/>
				: asset.name}
			</td>
			<td
			onClick={()=>props.handleDescriptionEditInput(asset._id)}
			>{props.showDescription && props.editId === asset._id
				? 
				<FormInput
					defaultValue={asset.description}
					type={"text"}
					onBlur={(e)=>props.handleEditDescription(e, asset._id)}
				/>
				: asset.description}
			</td>




			<td
			onClick={()=>props.handleCategoryEditInput(asset._id)}
			>{props.showCategory 
				&& props.editId 
				=== asset._id 
				?  
				<Dropdown isOpen={dropdownOpen} toggle={toggle}>
					<DropdownToggle caret>{asset.category}</DropdownToggle>
					<DropdownMenu>
						<DropdownItem
							onClick={()=>props.handleEditCategory("Vegetables")}
						>Vegetables</DropdownItem>
						<DropdownItem
							onClick={()=>props.handleEditCategory("Tools")}
						>Tools</DropdownItem>

						<DropdownItem
							onClick={()=>props.handleEditCategory("Camel")}
						>Camel</DropdownItem>
					</DropdownMenu>
				</Dropdown>
				: asset.category}</td>
			<td
				onClick={()=>props.handleStockEditInput(asset._id)}
			>{props.showStock && props.editId === asset._id
				? 
				<FormInput 
					defaultValue={asset.stock}
					type={"number"}
					onBlur={(e)=>props.handleEditStock(e, asset._id)}
				/> 
				: asset.stock} 
			</td>
			<td>
			<Button
				color="danger"
				onClick={()=>props.handleDeleteAsset(asset._id)}
			>
				Remove Asset
			</Button></td>
			

		</tr>
		</React.Fragment>
	)
}

export default AssetRow;