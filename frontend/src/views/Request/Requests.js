import React, {useEffect, useState} from 'react';
import {RequestRow, AddForm} from './components';
import {Loading} from '../../globalcomponents';
import axios from 'axios';
import moment from 'moment';

import {
	Button
} from 'reactstrap';


const Requests = () =>{
	const [requests, setRequests] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm] =useState(false);
	const [assetName, setAssetName] = useState("");
	const [assetId, setAssetId]= useState("");
	const [quantity, setQuantity] = useState(0);
	const [ editId, setEditId] = useState("");
	const [ showStatus, setShowStatus]= useState(false);
	const [ showQuantity, setShowQuantity]= useState(false);



	const handleRefresh =() =>{
		setShowForm(false);
		setAssetName("");
		setAssetId("");
		setQuantity(1);
		setEditId("");

	}


		useEffect(()=>{
		
		axios.get('http://localhost:4000/admin/showrequests'
		).then(res=>{
			setRequests(res.data);
			console.log(res.data)
			setIsLoading(false);
		
		})
	},[]);


		const handleDeleteRequest = requestId =>{
			axios.delete('http://localhost:4000/admin/deleterequest/'+requestId
				).then(res=>{
					let index = requests.findIndex(request=>request._id===requestId);
					let newRequests = [...requests]
					newRequests.splice(index,1)
					setRequests(newRequests)
					
				})
		}

		const handleShowAddForm =() =>{
			setShowForm(!showForm);
		}

		const handleAssetInput = asset =>{
			setAssetName(asset.name)
			setAssetId(asset._id)
		}

		const handleQuantityInput =(e)=>{
		setQuantity(e.target.value)
		
	}

			

		const handleShowStatus = (editId)=>{
			setEditId(editId);
			setShowStatus(true)
			setShowQuantity(false)
		}

			const handleShowQuantity = (editId)=>{
			setEditId(editId);
			setShowStatus(false)
			setShowQuantity(true)
		}





		const handleSaveRequest = ()=>{
			let code = moment(new Date).format("x");
			//this is hardcoded, we will refactor this later
			let requestor = "Burlington"
			let requestorId = "463"
			let approver = "Alfredo"
			let approverId = ":("

			axios.post('http://localhost:4000/admin/addrequest',{
				code: code,
				quantity: quantity,
				assetName: assetName,
				assetId: assetId,
				requestor: requestor,
				requestorId: requestorId,
				approver: approver,
				approverId: approverId

			}).then(res=>{
				let newRequests = [...requests]
				newRequests.push(res.data)
				setRequests(newRequests)
				handleRefresh();
			})
		}


		const handleStatusInput = (status, logs)=>{
			let log = {
				status: "pending",
				updateDate: moment(new Date).format("MM/DD/YYYY"),
				message: "Updated Status to " +status
			}

			axios.patch('http://localhost:4000/admin/updaterequeststatus/'+editId,
				{status,
				logs: log

			}).then(res=>{
				let index = requests.findIndex(request=>request._id==editId);
				let newRequests = [...requests];
				newRequests.splice(index,1,res.data);
				setRequests(newRequests);
				handleRefresh();
			})
		}

		const handleEditQuantityInput =(e,request)=>{
			let oldQuantity = request.quantity;
			let quantity = e.target.value;
			let log = {
				status: "pending",
				updateDate: moment(new Date).format("MM/DD/YYYY"),
				message: " Quantity updated from "+ oldQuantity +" to " + quantity
			}

			axios.patch('http://localhost:4000/admin/updaterequestquantity/'+editId,{
				quantity,
				logs: log

			}).then(res=>{
				let index = requests.findIndex(request=>request._id==editId)
				let newRequests = [...requests]
				newRequests.splice(index,1,res.data);
				setRequests(newRequests);
				handleRefresh();
			})
		}




	return(
		<React.Fragment>
			{isLoading ?
			<Loading />
		:
		<React.Fragment>
		<h1 
		className="text-center py-5">Requests</h1>

				<div className="d-flex justify-content-center py-3 pb-4">
					<Button
					color="success"
					onClick={handleShowAddForm}
					>+ Add Request</Button>


					<AddForm
					showForm={showForm}
					handleShowAddForm={handleShowAddForm}
					handleAssetInput={handleAssetInput}
					assetName={assetName}
					handleQuantityInput={handleQuantityInput}
					quantity={quantity}
					handleSaveRequest={handleSaveRequest}
					/>

					
		
					
				</div>
			<div className="vh-100">
			<table
				className="table table-striped border col-lg-10 offset-lg-1"
			>
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Date Requested</th>
						<th>Asset Requested</th>
						<th>Quantity</th>
						<th>Requested By</th>
						<th>Status</th>
						<th>Approved By</th>
						<th>Actions</th>
					

					</tr>
				</thead>
				<tbody>
					{requests.map(request=>
						<RequestRow 
							key={request._id}
							request={request}
							handleDeleteRequest={handleDeleteRequest}
							handleShowStatus={handleShowStatus}
							showStatus={showStatus}
							editId={editId}
							handleStatusInput={handleStatusInput}
							handleShowQuantity={handleShowQuantity}
							showQuantity={showQuantity}
							handleEditQuantityInput={handleEditQuantityInput}
							

							
						/>
					)}
			
				</tbody>
			</table>

			</div>
			</React.Fragment>
			}
		</React.Fragment>
		)
}

export default Requests;