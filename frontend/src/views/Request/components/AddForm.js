import React from 'react';

import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';

import {FormInput} from '../../../globalcomponents';

import {AssetInput} from '../components';

const AddForm = props =>{
	return(
		<React.Fragment>
		<Modal
		isOpen={props.showForm}
		toggle={props.handleShowAddForm}

		>
		<ModalHeader
		toggle={props.handleShowAddForm}
		style={{"backgroundColor":"indigo", "color":"pink"}}
		>Add Request</ModalHeader>
		<ModalBody>
		<AssetInput
		handleAssetInput={props.handleAssetInput}
		assetName={props.assetName}
		/>
		<FormInput
		name={"quantity"}
		label={"Quantity"}
		type={"number"}
		placeholder={"Input Quantity"}
		onChange={props.handleQuantityInput}
		defaultValue={0}

			/>

			<Button color="warning"
			block
			disabled={props.assetName== "" || props.quantity <= 0 ? true : false}
			onClick={props.handleSaveRequest}
			>Add Request</Button>
			<Button>Cancel</Button>
					
		</ModalBody>
		</Modal>
		</React.Fragment>
		)
}

export default AddForm;